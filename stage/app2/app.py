# update test
import os

def print_script_path():
    script_path = os.path.dirname(os.path.realpath(__file__))
    print(f"Script Path: {script_path}")

if __name__ == "__main__":
    print_script_path()
